#!/bin/zsh
mkdir -p ~/.ssh
mkdir -p ~/.gnupg
mkdir -p ~/.vim/bundle
stow git gpg ssh tmux vim zsh
