set -g default-terminal "screen-256color"
#set -g default-terminal "rxvt-unicode-256color"
#set -g default-shell "/bin/zsh"

# Screen key bindings
unbind C-b
set -g prefix C-a
bind a send-prefix

# set scrollback history to 10000 (10k)
set-option -g history-limit 10000

# set window and pane index to 1 (0 by default)
set-option -g base-index 1
set-window-option -g pane-base-index 1

# use PREFIX | to split window horizontally and PREFIX - to split vertically
bind | split-window -h -c "#{pane_current_path}"
bind - split-window -v -c "#{pane_current_path}"

# When opening new window set current dir
bind c new-window -c "#{pane_current_path}"

set-option -g renumber-windows on

bind-key y setw synchronize-panes

bind r source-file ~/.tmux.conf \; display "Reloaded .tmux.conf"

###########################################################################
# Mouse mode and scroll options

#set -g mode-mouse on
#set -g mouse-resize-pane on
#set -g mouse-select-pane on
#set -g mouse-select-window on
set-option -g -q mouse on
#set-option -g mouse-utf8 off

bind-key -T root PPage if-shell -F "#{alternate_on}" "send-keys PPage" "copy-mode -e; send-keys PPage"
#bind-key -t vi-copy PPage page-up
#bind-key -t vi-copy NPage page-down

bind-key -T root WheelUpPane if-shell -F -t = "#{alternate_on}" "send-keys -M" "select-pane -t =; copy-mode -e; send-keys -M"
bind-key -T root WheelDownPane if-shell -F -t = "#{alternate_on}" "send-keys -M" "select-pane -t =; send-keys -M"
#bind-key -t vi-copy WheelUpPane halfpage-up
#bind-key -t vi-copy WheelDownPane halfpage-down


# Toggle mouse on
bind m \
    set -g -q mouse on \;\
    display 'Mouse: ON'

# Toggle mouse off
bind M \
    set -g -q mouse off \;\
    display 'Mouse: OFF'
