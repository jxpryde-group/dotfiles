#
# User configuration sourced by interactive shells
#

# Source zim
if [[ -s ${ZDOTDIR:-${HOME}}/.zim/init.zsh ]]; then
  source ${ZDOTDIR:-${HOME}}/.zim/init.zsh
fi

export EDITOR='vim'
export VISUAL='vim'
export PAGER='less'
export LESS='-g -i -M -R -S -w -z-4'

alias pacman=yay
alias g++='g++ -Wall -Wextra -Wpedantic'
alias clang++='clang++ -Wall -Wextra -Wpedantic'

if [[ -z "$LANG" ]]; then
  export LANG='en_CA.UTF-8'
fi

typeset -gU cdpath fpath mailpath path

path=(
  ~/.local/{bin,sbin}
  /usr/local/{bin,sbin}
  $path
  /usr/sbin
  /sbin
)

if [[ ! -d "$TMPDIR" ]]; then
  export TMPDIR="/tmp/$LOGNAME"
  mkdir -p -m 700 "$TMPDIR"
fi

TMPPREFIX="${TMPDIR%/}/zsh"

# SSH keychain
eval $(keychain --eval --noask -Q --quiet)

# Enable colours in xfce4-terminal
if [[ "$TERM" == "xterm" ]] && [[ "$COLORTERM" == "xfce4-terminal" ]] && [[ "$TMUX" == "" ]]; then
  export TERM=xterm-256color
  exec zsh
fi

# Proper font rendering in Java apps
export _JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=lcd'

# Smart wildcard rename
autoload -U zmv
alias mmv='noglob zmv -W'

# disable ctrl-s to pause
stty -ixon
